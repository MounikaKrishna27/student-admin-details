from django.db import models

# Createfrom django.db import models  
class Crud1(models.Model):  
    Grades= models.IntegerField()  
    Sections= models.CharField(max_length=100)    
    Subjects = models.CharField(max_length=15)  
    class Meta:  
        db_table = "crud"
class Crud2(models.Model):
    Name=models.CharField(max_length=100)
    EnrollmentNumber=models.IntegerField(unique=True)
    Grade=models.CharField(max_length=100)
    Section=models.CharField(max_length=100)
    Email=models.EmailField(max_length=150)
    PhoneNumber=models.CharField(max_length=10)
    ProfileImage=models.ImageField()

